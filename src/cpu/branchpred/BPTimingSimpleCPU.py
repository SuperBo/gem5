# Copyright (c) 20017 The Regents of The University of Texas at Dallas
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Super Bo

from m5.params import *
from m5.proxy import *
from TimingSimpleCPU import TimingSimpleCPU
from BranchPredictor import *

class BPTimingSimpleCPU(TimingSimpleCPU):
    abstract = True

    @classmethod
    def setBTBEntries(cls, size=4096):
        cls.branchPred.BTBEntries = size

    @classmethod
    def setPredictorSize(cls, **kwargs):
        pass


class LocalBPTimingSimpleCPU(BPTimingSimpleCPU):
    branchPred = LocalBP(numThreads = Parent.numThreads)

    @classmethod
    def setPredictorSize(cls, **kwargs):
        cls.branchPred.localPredictorSize = kwargs.get('localSize', 2048)


class TournamentBPTimingSimpleCPU(BPTimingSimpleCPU):
    branchPred = TournamentBP(numThreads = Parent.numThreads)

    @classmethod
    def setPredictorSize(cls, **kwargs):
        cls.branchPred.localPredictorSize = kwargs.get('localSize', 2048)
        cls.branchPred.globalPredictorSize = kwargs.get('globalSize', 8192)
        cls.branchPred.choicePredictorSize = kwargs.get('choiceSize', 8192)


class BiModeBPTimingSimpleCPU(BPTimingSimpleCPU):
    branchPred = BiModeBP(numThreads = Parent.numThreads)

    @classmethod
    def setPredictorSize(cls, **kwargs):
        cls.branchPred.globalPredictorSize = kwargs.get('globalSize', 8192)
        cls.branchPred.choicePredictorSize = kwargs.get('choiceSize', 8192)
